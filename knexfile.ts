import "dotenv/config";
import path from "path";
import type { Knex } from "knex";

export type Environment = "development";

export const development: Knex.Config = {
  client: "postgresql",
  useNullAsDefault: true,
  connection: {
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    directory: path.resolve(__dirname, "src", "database", "migrations"),
    extension: "ts",
    loadExtensions: [".js", ".ts"],
  },
  seeds: {
    directory: path.resolve(__dirname, "src", "database", "seeds"),
    extension: "ts",
    loadExtensions: [".js", ".ts"],
  },
};
