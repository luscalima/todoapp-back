import "dotenv/config";
import { authRouter } from "../routes/authRouter";
import express from "express";
import cors from "cors";

const server = express();
const corsConfig = cors({
  credentials: true,
  origin: process.env.FRONTEND_URL,
});

server.use(express.json());
server.use(corsConfig);

// Routers
server.use(authRouter);

export { server };
