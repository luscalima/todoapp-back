import "dotenv/config";
import { Request, Response } from "express";
import { Controller } from "../interfaces/controllers/AuthController";
import { Repository } from "../interfaces/repositories/UserRepository";
import { User } from "../models/User";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const maxAge = 3 * 24 * 60 * 60;

export class AuthController implements Controller {
  constructor(private readonly repository: Repository) {}

  signup = async (req: Request, res: Response) => {
    const user: User = req.body;
    const salt = bcrypt.genSaltSync(5);
    user.password = bcrypt.hashSync(user.password, salt);

    try {
      await this.repository.save(user);
      return res.status(201).json({ message: "User created successfully" });
    } catch (error) {
      return res.status(400).json({ message: "Unable to create user" });
    }
  };

  login = async (req: Request, res: Response) => {
    const { username, password }: User = req.body;

    const user = await this.repository.findByUsername(username);

    if (!user) {
      return res.status(401).json({ message: "Invalid credentials" });
    }

    const isPasswordValid = bcrypt.compareSync(password, user.password);

    if (!isPasswordValid) {
      return res.status(401).json({ message: "Invalid credentials" });
    }

    const token = jwt.sign(
      {
        id: user.id,
        username: user.username,
        email: user.email,
      },
      process.env.JWT_SECRET!,
      {
        expiresIn: maxAge,
      }
    );

    const userSafeData = {
      username: user.username,
      email: user.email,
    };

    res.cookie("jwt", token, {
      httpOnly: true,
      maxAge: maxAge * 1000,
      sameSite: "strict",
    });
    res.status(200).json({ message: "Login successful", user: userSafeData });
  };
}
