import { Request, Response } from "express";
import { Controller } from "../interfaces/controllers/UserController";
import { Repository } from "../interfaces/repositories/UserRepository";
import { User } from "../models/User";
import bcrypt from "bcrypt";

export class UserController implements Controller {
  constructor(private readonly repository: Repository) {}

  create = async (req: Request, res: Response) => {
    const user: User = req.body;
    const salt = bcrypt.genSaltSync(5);
    user.password = bcrypt.hashSync(user.password, salt);
    user.salt = salt;

    try {
      await this.repository.save(user);
      return res.status(201).json({ message: "User created successfully" });
    } catch (error) {
      return res.status(400).json({ message: "Unable to create user" });
      // return res.status(400).send("Unable to create user");
    }
  };
}
