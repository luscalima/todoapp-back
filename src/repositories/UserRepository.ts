import { User } from "@/models/User";
import { Repository } from "../interfaces/repositories/UserRepository";
import { database } from "../database";
import { TableName } from "../database/tables";

export class UserRepository implements Repository {
  async save(user: User): Promise<void> {
    await database.insert(user, ["id"]).into(TableName.USERS);
  }

  async findByUsername(username: string): Promise<User | undefined> {
    const [user] = await database
      .select("*")
      .from(TableName.USERS)
      .where({ username });

    return user;
  }
}
