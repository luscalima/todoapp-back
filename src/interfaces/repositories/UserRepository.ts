import { User } from "../../models/User";

export interface Repository {
  save(user: User): Promise<void>;
  findByUsername(username: string): Promise<User | undefined>;
}
