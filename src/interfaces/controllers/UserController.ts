import { Request, Response } from "express";

export interface Controller {
  create(req: Request, res: Response): void;
}
