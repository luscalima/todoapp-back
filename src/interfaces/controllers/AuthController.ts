import { Request, Response } from "express";

export interface Controller {
  signup(req: Request, res: Response): void;
  login(req: Request, res: Response): void;
}
