import { UserController } from "../controllers/UserController";
import { UserRepository } from "../repositories/UserRepository";
import { Router } from "express";

const userRouter = Router();
const userRepository = new UserRepository();
const userController = new UserController(userRepository);

userRouter.post("/users", userController.create);

export { userRouter };
