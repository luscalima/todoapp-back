import { AuthController } from "../controllers/AuthController";
import { UserRepository } from "../repositories/UserRepository";
import { Router } from "express";

const authRouter = Router();
const userRepository = new UserRepository();
const userController = new AuthController(userRepository);

authRouter.post("/signup", userController.signup);
authRouter.post("/login", userController.login);

export { authRouter };
