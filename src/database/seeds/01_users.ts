import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("users").del();

  await knex("users").insert([
    {
      id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      name: "John Doe",
      username: "john_doe",
      email: "john@example.com",
      password: "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8",
    },
  ]);
}
