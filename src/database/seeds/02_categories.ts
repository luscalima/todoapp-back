import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("categories").del();

  await knex("categories").insert([
    { id: "658e068f-7939-4dba-83f6-2958aa891b6b", title: "Home" },
    { id: "89680095-6881-49b6-9bf4-71bd4834b666", title: "Work" },
    { id: "b8c297f0-c3d6-4d1c-b73f-28fb546821f6", title: "Personal" },
  ]);
}
