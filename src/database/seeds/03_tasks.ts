import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
  await knex("tasks").del();

  await knex("tasks").insert([
    {
      id: "5675ddce-bf50-438b-89b1-49334010e48c",
      title: "Complete homework",
      description: "Math and Science assignments",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "658e068f-7939-4dba-83f6-2958aa891b6b",
    },
    {
      id: "0cbc000c-86f4-4def-b372-4b997e0f5728",
      title: "Prepare presentation",
      description: "For the upcoming meeting",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "89680095-6881-49b6-9bf4-71bd4834b666",
    },
    {
      id: "f6459e33-c285-43d1-a667-065997160549",
      title: "Buy groceries",
      description: "Milk, eggs, bread",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "658e068f-7939-4dba-83f6-2958aa891b6b",
    },
    {
      id: "668fbc50-6765-4216-803c-23bda9363164",
      title: "Fix the leaking faucet",
      description: "It's been leaking for a while",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "658e068f-7939-4dba-83f6-2958aa891b6b",
    },
    {
      id: "09ac37b8-789a-46da-9781-5ae8f0939bcc",
      title: "Call mom",
      description: "Wish her a happy birthday",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "b8c297f0-c3d6-4d1c-b73f-28fb546821f6",
    },
    {
      id: "977ff973-31d9-4f50-922f-7c40a6cf2e2f",
      title: "Go for a jog",
      description: "Run in the park",
      completed: false,
      user_id: "8e97a22a-0065-4dd8-b0c1-e963b822520d",
      category_id: "b8c297f0-c3d6-4d1c-b73f-28fb546821f6",
    },
  ]);
}
