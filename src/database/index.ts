import { knex as _knex } from "knex";
import { Environment, development } from "../../knexfile";

const config = {
  development,
};

export const database = _knex(
  config[(process.env.NODE_ENV || "development") as Environment]
);
