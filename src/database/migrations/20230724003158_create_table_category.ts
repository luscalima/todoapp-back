import { Knex } from "knex";
import { TableName } from "../tables";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(TableName.CATEGORIES, (table) => {
    table.uuid("id", { primaryKey: true }).defaultTo(knex.fn.uuid());
    table.string("title", 255).notNullable();
    table.timestamps(true, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(TableName.CATEGORIES);
}
