import { Knex } from "knex";
import { TableName } from "../tables";

export async function up(knex: Knex) {
  return knex.schema.createTable(TableName.USERS, (table) => {
    table.uuid("id", { primaryKey: true }).defaultTo(knex.fn.uuid());
    table.string("name", 255).notNullable();
    table.string("username", 255).unique().notNullable();
    table.string("email", 255).unique().notNullable();
    table.string("password", 255).notNullable();
    table.timestamps(true, true);
  });
}

export async function down(knex: Knex) {
  return knex.schema.dropTable(TableName.USERS);
}
