import { Knex } from "knex";
import { TableName } from "../tables";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.alterTable(TableName.TASKS, (table) => {
    table
      .uuid("category_id")
      .references("id")
      .inTable("categories")
      .onDelete("CASCADE");
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.alterTable(TableName.TASKS, (table) => {
    table.dropColumn("category_id");
  });
}
