import { Knex } from "knex";
import { TableName } from "../tables";

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(TableName.TASKS, (table) => {
    table.uuid("id", { primaryKey: true }).defaultTo(knex.fn.uuid());
    table.string("title", 255).notNullable();
    table.string("description", 255).notNullable();
    table.boolean("completed").defaultTo(false);
    table.uuid("user_id").references("id").inTable("users").onDelete("CASCADE");
    table.timestamps(true, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(TableName.TASKS);
}
