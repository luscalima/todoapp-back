export enum TableName {
  USERS = "users",
  TASKS = "tasks",
  CATEGORIES = "categories",
}
